﻿using UnityEngine;
using System.Collections;
using System;

public class ScreenMainMenu : ScreenView
{
    public event Action OnNewGame = delegate { };
    public event Action OnOptions = delegate { };

    public void OnNewGameButton()
    {
        OnNewGame();
    }

    public void OnOptionButton()
    {
        OnOptions();
    }
}
