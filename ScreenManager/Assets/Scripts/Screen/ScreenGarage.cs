﻿using UnityEngine;
using System.Collections;
using System;

public class ScreenGarage : ScreenView
{
    public event Action OnUpgrade = delegate { };
    public event Action OnHireRacer = delegate { };
    public event Action OnSearchRace = delegate { };

    public void OnUpgradeButton()
    {
        OnUpgrade();
    }

    public void OnHireRacerButton()
    {
        OnHireRacer();
    }

    public void OnSearchRaceButton()
    {
        OnSearchRace();
    }
}
