﻿using UnityEngine;
using System.Collections;
using System;

public class ScreenView : MonoBehaviour {
    public event Action OnExit = delegate { };

    protected virtual void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Exit();
    }

    public void OnBackButton()
    {
        Exit();
    }

    protected void Exit()
    {
        OnExit();
    }
}
