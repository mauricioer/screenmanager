﻿using UnityEngine;
using System.Collections;

public class ScreenManager : MonoBehaviour
{
    public ScreenView firstScreen;
    public ScreenMainMenu screenMainMenu;
    public ScreenOptions screenOptions;
    public ScreenGarage screenGarage;
    public ScreenVehicleUpgrades screenVehicleUpgrades;
    public ScreenHireRacer screenHireRacer;
    public ScreenSearchRace screenSearchRace;
    private ScreenView _currentScreen;

    private void Awake()
    {
        foreach (var screen in GetComponentsInChildren<ScreenView>()) screen.gameObject.SetActive(false);
        GoToScreen(firstScreen);
        ConfigureScreens();
    }

    public void GoToScreen(ScreenView screen)
    {
        if (_currentScreen) _currentScreen.gameObject.SetActive(false);
        screen.gameObject.SetActive(true);
        _currentScreen = screen;
    }

    private void ConfigureScreens()
    {
        //Main menu
        screenMainMenu.OnNewGame += () => GoToScreen(screenGarage);
        screenMainMenu.OnOptions += () => GoToScreen(screenOptions);

        //Options
        screenOptions.OnExit += () => GoToScreen(screenMainMenu);

        //Garage
        screenGarage.OnHireRacer += () => GoToScreen(screenHireRacer);
        screenGarage.OnSearchRace += () => GoToScreen(screenSearchRace);
        screenGarage.OnUpgrade += () => GoToScreen(screenVehicleUpgrades);
        screenGarage.OnExit += () => GoToScreen(screenMainMenu);

        //Vehicle Upgrades
        screenVehicleUpgrades.OnExit += () => GoToScreen(screenGarage);

        //Hire Racer
        screenHireRacer.OnExit += () => GoToScreen(screenGarage);

        //Search Race
        screenSearchRace.OnExit += () => GoToScreen(screenGarage);
    }
}
