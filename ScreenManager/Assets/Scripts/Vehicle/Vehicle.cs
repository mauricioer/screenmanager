﻿using UnityEngine;
using System.Collections;

public class Vehicle : MonoBehaviour
{
    public float minimumTurn, maximumTurn;
    protected Rigidbody _rb;

    protected void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    protected void Update()
    {
        transform.position += transform.forward * 20 * Time.deltaTime;
        ApplySteering(transform.InverseTransformDirection(_rb.velocity));
    }

    protected void ApplySteering(Vector3 relativeVelocity)
    {
        float turnRadius = 3f / Mathf.Sin((90 - (Input.GetAxis("Horizontal") * 30)) * Mathf.Deg2Rad);
        float minMaxTurn = EvaluateSpeedToTurn(_rb.velocity.magnitude);
        float turnSpeed = Mathf.Clamp(relativeVelocity.z / turnRadius, -minMaxTurn / 10, minMaxTurn / 10);
        transform.RotateAround(transform.position + transform.right * turnRadius * Input.GetAxis("Horizontal"), transform.up,
                                turnSpeed * Mathf.Rad2Deg * Time.deltaTime * Input.GetAxis("Horizontal"));
    }
    protected float EvaluateSpeedToTurn(float speed)
    {
        if (speed > 180 / 2) return minimumTurn;
        var speedIndex = 1 - (speed / (180 / 2));

        return minimumTurn + speedIndex * (maximumTurn - minimumTurn);
    }
}
